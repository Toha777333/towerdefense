﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MonsterLevel
{
    public int cost;
    public GameObject visualization;
    public GameObject bullet;
    public float fireRate;
}

public class MonsterData : MonoBehaviour {

    public List<MonsterLevel> levels;
    private MonsterLevel currentLevel;

    public MonsterLevel CurrentLevel
    {
        get
        {
            Debug.Log("return currentLevel");
            return currentLevel;
        }
        set
        {
            currentLevel = value;
            Debug.Log("1 - " + currentLevel.cost);

            int currentLevelIndex = levels.IndexOf(currentLevel);
            Debug.Log(" 2 currentLevelIndex - " + currentLevelIndex);

            GameObject levelVisualization = levels[currentLevelIndex].visualization;
            for (int i = 0; i < levels.Count; i++)
            {
                Debug.Log("for" + i);
                if (levelVisualization != null)
                {
                    Debug.Log("3 levelVisualization != null");
                    if (i == currentLevelIndex)
                    {
                        Debug.Log("4 true" + i + "_" + currentLevelIndex);
                        levels[i].visualization.SetActive(true);
                    }
                    else
                    {
                        Debug.Log("4 false" + i + "_" + currentLevelIndex);
                        levels[i].visualization.SetActive(false);
                    }
                }
            }
        }
    }

    void OnEnable()
    {
        CurrentLevel = levels[0];
    }

    public MonsterLevel GetNextLevel()
    {
        int currentLevelIndex = levels.IndexOf(currentLevel);
        int maxLevelIndex = levels.Count - 1;
        if (currentLevelIndex < maxLevelIndex)
        {
            if (levels[currentLevelIndex + 1] != null)
            {
                return levels[currentLevelIndex + 1];
            }
            else
            {
                return null;
            }
        }
        else
        {
            return null;
        }
    }

    public void IncreaseLevel()
    {
        int currentLevelIndex = levels.IndexOf(currentLevel);
        if (currentLevelIndex < levels.Count - 1)
        {
            CurrentLevel = levels[currentLevelIndex + 1];
        }
    }

}
